# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-11-16 22:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Ubay González <u.gonzalez@gmail.com>, 2017\n"
"Language-Team: English (https://www.transifex.com/semilla-social/teams/65146/en/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/settings/common.py:308
msgid "Spanish"
msgstr "Spanish"

#: config/settings/common.py:309
msgid "English"
msgstr "English"

#: landing/templates/landing/contact.html:7
msgid "Contacto"
msgstr "Contact"

#: landing/templates/landing/contact.html:14
msgid "Puedes contactarnos en"
msgstr "Contact us:"

#: landing/templates/landing/contact.html:18
msgid "También puedes conocernos en "
msgstr "You can also learn about us here: "

#: landing/templates/landing/contact.html:18
msgid "este link"
msgstr "this link"

#: landing/templates/landing/development.html:8
msgid "Version 0.1"
msgstr "Version 0.1"

#: landing/templates/landing/home.html:9
msgid "App móvil de compraventa de bienes y servicios con monedas virtuales."
msgstr ""
"Mobile App for selling and purchasing goods and services with virtual "
"currencies"

#: landing/templates/landing/home.html:22
msgid "Marketplace Social "
msgstr "Social Marketplace "

#: landing/templates/landing/home.html:23
msgid ""
"Se basa en la abundancia de recursos, la capacidad creativa, artística, "
"manual e intelectual de todos sus miembros."
msgstr ""
"It's based on the abundance of resources and the creative, artistic, manual "
"and intellectual capabilities of all its members. "

#: landing/templates/landing/home.html:24
msgid "Los bienes y servicios ofertados aparecen ordenados por proximidad"
msgstr "The goods and services offered appear in order of proximity."

#: landing/templates/landing/home.html:42
msgid "Democrática"
msgstr "Democratic"

#: landing/templates/landing/home.html:43
msgid "Las decisiones se toman por votación entre los miembros"
msgstr "Decisions are taken by consensus among the members"

#: landing/templates/landing/home.html:44
msgid "Ven y Participa"
msgstr "Come and join us"

#: landing/templates/landing/home.html:59
msgid "Moneda Social "
msgstr "Social Currency"

#: landing/templates/landing/home.html:60
msgid ""
"Semillas promociona el bien común. Retribuyendo en semillas trabajos a la "
"comunidad o acciones cívicas."
msgstr ""
"\"Semillas\" promotes the common good. Seeds are given in exchange for "
"community work or civic duties. "

#: landing/templates/landing/how_it_works.html:9
#: semillas_backend/templates/base.html:105
#: semillas_backend/templates/base.html:106
msgid "Cómo Funciona"
msgstr "How it works"

#: landing/templates/landing/how_it_works.html:10
msgid ""
"Tú empiezas con una cuenta de 0 semillas, pero ofreciendo algo (un servicio "
"o un producto). Cuando alguien lo quiere, te paga con semillas, de forma que"
" tú sumas semillas en tu cuenta y ella se las resta de la suya. Por ejemplo,"
" tú ofreces naranjas a 3 semillas el kg. A Julia, que le encantan las "
"naranjas, le interesa la oferta, y quiere 1 kg. Tú le das las naranjas y te "
"sumas 3 semillas. Julia se restará 3 semillas. Y esta es la forma de "
"conseguir semillas!!"
msgstr ""
"You start with 0 Semillas in your account, but offering something (a service"
" or a product). When someone wants it, she pays you with Semillas, so you "
"increase your Semillas account while she removes Semillas from her account. "
"For example, you are offering oranges 3 Semillas/Kg. Julia, who loves "
"oranges, is interested in your offer, and wants 1 Kg. You give her the "
"oranges and she will transfer you 3 Semillas. And that's the way of getting "
"Semillas. "

#: landing/templates/landing/how_it_works.html:14
msgid "Entonces, ¿puedo tener negativa la cuenta de semillas?"
msgstr "Then, can my Semillas account be negative?"

#: landing/templates/landing/how_it_works.html:16
#, python-format
msgid ""
"\n"
"      Claro que puedes. Hay un límite, actualmente es %(limit)s, del que ya no podrás pasar. Así que tendrás que ofrecer algo que interese a la gente para poder equilibrar tu cuenta. No te preocupes, seguro que surge algo que tú puedes hacer por ell@s, y poder sumar semillas. La semilla está pensada para que las usuarias vayamos pasando constantemente de saldos negativos a positivos y viceversa. Un saldo positivo indica que hemos aportado a la red más de lo que hemos consumido, mientras que un saldo negativo indica que hemos consumido de la red más de lo que hemos aportado.\n"
"      "
msgstr ""
"\n"
"Of course. There is a limit, currently is %(limit)s, that you cannot overpass. So you will have to offer something that interest people in order to balance your account. Don't worry, for sure you can do something for someone and add Semillas yo your account. The Semilla is designed so the users keep moving from positive balance to negative and back. A positive balance means that we have given to the network more than we have consumed, while a negative balance means that we have consumed more than we have given. "

#: landing/templates/landing/how_it_works.html:23
msgid "Redistribución Mensual"
msgstr "Monthly redistribution"

#: landing/templates/landing/how_it_works.html:25
#, python-format
msgid ""
"\n"
"      Mensualmente se aplicará una redistribución de la Semilla del 5%%. A quienes tengan saldo positivo, se les restará un 5%% de la cantidad, mientras que si tu saldo es negativo, se te sumará un 5%% de la cantidad negativa que tengas. Con el paso del tiempo todas las cuentas convergen a 0.\n"
"      "
msgstr ""
"\n"
"A monthly redistribution of 5%% will be applied. Those who have a positive balance, a 5%% of their Semillas account will be removed, while those who have a negative balance, will get a 5%% number of Semillas of the negative quantity you have. Abandoned accounts will converge to zero after some time. "

#: landing/templates/landing/how_it_works.html:33
msgid "Valores de Referencia"
msgstr "Reference values"

#: landing/templates/landing/how_it_works.html:35
msgid ""
"\n"
"      La referencia decidida para los servicios es de 10 Semillas/hora, siempre de manera orientativa. No es que vaya a haber nadie controlando ni que esto no pueda variar algo en cada caso. No obstante, aplicando todas el mismo índice podemos asegurar que el sistema funciona de manera equitativa.\n"
"      "
msgstr ""
"\n"
"The reference decided for the services is of 10 Semillas/hour, this is an guidance value. No one will be controlling, and this can vary. But applying this index we can make sure the system works i an equitable manner. "

#: landing/templates/landing/how_it_works.html:43
msgid "Precios en Semillas y en Euros"
msgstr "Prices in Semillas and Euros"

#: landing/templates/landing/how_it_works.html:45
msgid ""
"\n"
"        Pueden existir artículos donde puede ser preciso pagar un porcentaje en Semillas y un porcentaje en Euros. Esto es debido a que el prosumidor tiene unos gastos fijos en Euros para poder ofrecer dichos bienes y servicios.\n"
"      "
msgstr ""
"\n"
"There can exists products where can be interesting to pay a percentage in Semillas and a percentage in official currency. This is because the user has some fixed expenses in official currency required to offer those goods or services. "

#: landing/templates/landing/how_it_works.html:52
msgid "Únete a nosotros y ayúdanos a decidir"
msgstr "Join us and help us to take decisions! "

#: landing/templates/landing/how_it_works.html:54
msgid ""
"\n"
"      Somos un equipo deslocalizado. Queremos ser más abiertos y asamblearios, estamos en proceso. Únete a nosotros en este camino. Escribe a info.semillasocial@gmail.com.\n"
"      "
msgstr ""
"\n"
"We are a remote team. We are in our way to be more open and assembly oriented. Join us in this change. Write to info.semillasocial.org@gmail.com"

#: landing/templates/landing/porque.html:7
msgid "Misión"
msgstr "Mission"

#: landing/templates/landing/porque.html:8
msgid ""
" Semillas es una tecnología abierta y transparente al servicio de lo humano."
" Es una herramienta de cambio social por su potencia para construir redes "
"entre iguales por donde circula, se distribuye y se intercambia la "
"abundancia de recursos, la capacidad creativa, artística, manual e "
"intelectual de todos sus miembros."
msgstr ""
"\"Semillas\" is an open and transparent technology, which serves humanity. "
"It's a tool for social change, due to its ability to build networks wherever"
" it circulates. In this way, the abundance of creative, artistic, manual and"
" intellectual resouces is distributed and exchanged though the support of "
"all its members. "

#: landing/templates/landing/porque.html:13
msgid ""
" La Semilla otorga soberanía local,  está sujeta a los procesos "
"participativos y democráticos de las personas que usan la moneda y sus "
"relaciones locales."
msgstr ""
"The \"seed\" grants local sovereignty. It is subject to the participative "
"and democratic processes of the people who use this currency, and their "
"local relationships."

#: landing/templates/landing/porque.html:16
msgid "Social"
msgstr "Social"

#: landing/templates/landing/porque.html:19
msgid ""
"\n"
"                Porque las personas que la hemos creado queremos visibilizar la economía de cuidados y empoderar a cada individuo en sus habilidades. A través de las categorías para crear servicios en la app queremos inferir un uso social, solidario, sostenible y ecológico.\n"
"                "
msgstr ""
"\n"
"Because the people that have created this, want to add visibility to the care economy and empower every individual in their skills. We are achieving this through the categories for creating new services in the app, we want to infer a social, solidarity, sustainable and ecologic use.  "

#: landing/templates/landing/porque.html:25
msgid ""
"\n"
"                También queremos permitir la creación de moneda para pagar acciones sociales que beneficien al común. Buscamos organos o asociaciones que quieran realizar trabajos para el beneficio de todos y retribuirlo en Semillas (como por ejemplo arreglar su centro social).\n"
"                "
msgstr ""
"\n"
"Also we want to allow the creation of new monetary mass to pay social actions that benefits the common. We are in search of asociations or institutions that want to perform jobs for the benefit of everyone and pay it in Semillas (like for example, to fix their social center). "

#: landing/templates/landing/porque.html:28
msgid "Ir al Banco de Semillas"
msgstr "Go to Semillas Bank"

#: landing/templates/landing/porque.html:31
msgid "Software Libre"
msgstr "Freeware"

#: landing/templates/landing/porque.html:32
msgid ""
" Todo el código que escribimos lo publicamos instantáneamente en Internet:"
msgstr "All the code we write is immediately published on the Internet."

#: landing/templates/landing/porque.html:33
msgid ""
" Si alguien modifica el producto, está obligado también a publicarlo en "
"internet, pudiendo el resto de desarrolladores adoptar las mejoras."
msgstr ""
"If someone modifies the product, they are also obligated to publish it on "
"the Internet, allowing the rest of the developers to also adopt the "
"improvements."

#: landing/templates/landing/porque.html:34
#, python-format
msgid " Por tanto es 100%% redistribuible, copiable y modificable."
msgstr "Therefore is 100%% redistributable, copyable and modifiable. "

#: landing/templates/landing/porque.html:35
msgid " No se pagan licencias por su uso."
msgstr "There's no need to pay user license fees."

#: landing/templates/landing/porque.html:36
msgid ""
" Cualquier entidad podría crearse su propia moneda utilizando este software."
msgstr "Any entity could create its own currency using this software."

#: landing/templates/landing/porque.html:44
msgid "Crédito Mutuo"
msgstr "Mutual Credit"

#: landing/templates/landing/porque.html:45
msgid ""
" Semillas es un sistema de crédito mutuo. La suma de las cantidades "
"positivas y negativas de todos sus miembros es 0. Propone una oxidación "
"equitativa para incentivar el uso y evitar la acumulación."
msgstr ""
"Semillas is a mutual credit system. The total sum of every positive balance "
"accound and all the negative one is 0. It proposes a equitative oxidation to"
" boost the use and avoid accumulation. "

#: landing/templates/landing/porque.html:46
msgid ""
" Las Semillas cobran existencia en el mismo momento en el que dos personas o"
" colectivos aceptan libremente un intercambio: a una se le suma el valor "
"convenido en semillas y a otra se le resta esa misma cantidad."
msgstr ""
"The seeds come to life in the same moment that two people or collectives "
"voluntarily accept an exchange: one adds the seed value decided upon and the"
" other subtracts the same amount."

#: landing/templates/landing/porque.html:47
msgid ""
" Un saldo positivo indica que hemos aportado a la red más de lo que hemos "
"consumido, mientras que un saldo negativo indica que hemos consumido de la "
"red más de lo que hemos aportado. Así, cuando nuestro saldo sea negativo "
"ofreceremos algún producto o servicio para ponernos en positivo, y cuando "
"estemos en positivo consumiremos y reduciremos hasta valores negativos. "
msgstr ""
"A positive balance indicates that we have contributed more than we have "
"consumed, while a negative balance indicates that we have consumed more than"
" we have contributed to the network. Therefore, when our balance is "
"negative, we can offer some product or service to make it positive again. "

#: landing/templates/landing/porque.html:52
msgid "Plug & Play"
msgstr "Plug & Play"

#: landing/templates/landing/porque.html:53
msgid ""
" A pesar de que hay diferentes grupos y experimentos con iniciativas "
"similares, es muy complicado empezar a formar parte de ellas. Hay que enviar"
" correos, ir a colectivos el día de asamblea. En Semillas darse de alta va a"
" ser cuestión de segundos. "
msgstr ""
"Despite the fact that there are different groups and experiments using "
"similar iniciatives, it's very complicated to get involved in them. You must"
" send e-mails, attend collective meetings, etc. In \"Semillas\" you can "
"become an active member in a matter of seconds."

#: landing/templates/landing/porque.html:55
msgid "Local & Global"
msgstr "Local & Global"

#: landing/templates/landing/porque.html:56
msgid ""
" El orden de las ofertas será por proximidad. La proximidad hará que se "
"favorezca al mercado local. Sin embargo, queremos dar una posibilidad "
"tecnológica para poder obtener, a cambio de tus semillas, clases de inglés "
"al otro lado del océano. "
msgstr ""
"All the offers are sorted by proximity, which will favor the local market. "
"But we want to give a technological possibility to get, in exchange for your"
" Semillas, Chinese classes from China, for example.  "

#: landing/templates/landing/porque.html:64
msgid "Moneda Colaborativa"
msgstr "Collaborative Currency"

#: landing/templates/landing/porque.html:65
msgid ""
" Nuestra propuesta no es solo una moneda alternativa, si no también una "
"herramienta para crear redes colaborativas. Propone sinergias con "
"plataformas de economía colaborativa que acepten pago en semillas."
msgstr ""
"Our proposal isn't only an alternative currency, but also a tool for "
"creating collaborative networks. It promotes synergies with collaborative "
"economy platforms, which accept payment in seeds. "

#: landing/templates/landing/porque.html:69
msgid "Catalizador de Sobreranía"
msgstr "Sovereignty Catalyst"

#: landing/templates/landing/porque.html:70
msgid ""
" Semillas Social promociona el bien común. Retribuyendo en semillas trabajos"
" a la comunidad o acciones cívicas.Para ello se apoya en los “Bancos de "
"Semillas” "
msgstr ""
"Semillas promotes the common good, Paying in Semillas works to the community"
" or civic actions. For that it relies on Semillas Banks. "

#: landing/templates/landing/porque.html:73
msgid "Banco de semillas"
msgstr "Seed Banks"

#: landing/templates/landing/porque.html:74
msgid " Es un grupo vecinal al cobijo de una institución pública o privada."
msgstr ""
"It's a neighborhood group in the shelter of a public or private institution."

#: landing/templates/landing/porque.html:75
msgid ""
" El Banco de Semillas se encarga de crear y retribuir trabajos a la "
"comunidad (índole social, ecológica, educativa, medioambiental) así como "
"acciones cívicas."
msgstr ""
"The Seed Bank is in charge of creating and rewarding community work done "
"(social, ecological, educational, environmental), as well as civic duties. "

#: landing/templates/landing/porque.html:76
msgid ""
" El siguiente ejemplo muestra ayuntamiento coordinando un banco de semillas."
" El ayuntamiento cierra el ciclo monetario aceptando La Semilla en el "
"alquiler de polideportivos, pago de multas, etc. "
msgstr ""
"The next example shows the city hall coordinating a Seed Bank. The city hall"
" closes the monetary cycle by accepting The Seed as payment in the rental of"
" sport centers, fines, etc. "

#: landing/templates/landing/who_we_are.html:8
msgid "Conoce por qué nos une Semilla Social"
msgstr "Know why we are drawn together by Semilla Social"

#: landing/templates/landing/who_we_are.html:10
msgid ""
"Semillas es una plataforma transparente y sin ánimo de lucro de intercambio "
"de bienes y servicios donde se puede participar sin moneda de curso legal. "
"Somos un grupo de hacktivistas, no hacemos esto por dinero. "
msgstr ""
"Semillas is a transparent and non-lucrative platform for exchanging goods "
"and service, where everyone can participate without an official currency. We"
" are a group of hacktivists, we don't do this for money. "

#: landing/templates/landing/who_we_are.html:11
msgid ""
"Nos organizamos a través del modelo cooperativo y como comunidad de software"
" libre para dar soporte a las personas, a los grupos, empresas del sector "
"público y/o privado que quieran como nosotros fomentar valores de igualdad y"
" solidaridad en una red de intercambios recíprocos. "
msgstr ""
"We organize ourselves according to a cooperative model, and as a freeware "
"community, to give support to the people, groups, companies of the public or"
" private sector who want, like us, to strengthen their values of equality "
"and solidarity through a network of reciprocity"

#: landing/templates/landing/who_we_are.html:12
msgid ""
"Buscamos todo tipo de ayuda. Ahora especialmente diseñadores, Programadores "
"(Javascript y Python), divulgadores, economistas, abogados, traductores, "
"amigos que nos traigan frutos secos y semillas "
msgstr ""
"We are looking for any kind of help, designers, developers, lawyers, "
"translators, friends! "

#: landing/templates/landing/who_we_are.html:16
msgid "Por qué ser parte del proyecto"
msgstr "Why should you be take part in Semilla Social"

#: landing/templates/landing/who_we_are.html:19
msgid ""
"Porque quieres contribuir al software libre y trabajar con un equipo de "
"personas muy simpáticas y muy motivadas ayudándote."
msgstr ""
"Because you want to contribute to open source and be a part of an easy going"
" team , motivated in helping each other."

#: landing/templates/landing/who_we_are.html:20
msgid ""
"Porque hacemos esto para aprender, usar las últimas tecnologías, reciclarnos"
" y disfrutar."
msgstr ""
"Because we do this to learn, to use the latest technologies, to recycle "
"ourselves and enjoy"

#: landing/templates/landing/who_we_are.html:21
msgid ""
"Porque queremos dejar la situación socioeconómica mejor de lo que la hemos "
"encontrado."
msgstr ""
"Because we want to leave the socioeconomical situation better than we found "
"it."

#: landing/templates/landing/who_we_are.html:22
msgid ""
"Porque vamos a hacer un producto precioso y hecho con amor. Donde se van a "
"intercambiar las cosas bonitas que fabrican las personas de nuestro "
"alrededor."
msgstr ""
"Because we are going to make a wonderful product, made with love. Where we "
"are going to exchange all the nice things that people around us make and "
"create."

#: landing/templates/landing/who_we_are.html:27
msgid "Personas que colaboran en Semilla Social"
msgstr "Semilla social team"

#: landing/templates/landing/who_we_are.html:33
msgid "Necesitamos gente"
msgstr "We need people"

#: landing/templates/landing/who_we_are.html:34
msgid "¿Quieres participar en el proyecto?"
msgstr "Do you want to be a part of the proyect? Join us!"

#: landing/templates/landing/who_we_are.html:39
msgid ""
"Desarrollador Back-end. Interesado en economía, política, educación y "
"tecnología."
msgstr ""
"Back-end developer. Interests include: Economics, Politics, Education and "
"Technology."

#: landing/templates/landing/who_we_are.html:45
msgid ""
"Fullstack developer interested in leveraging technology for Social projects"
msgstr "Full Stack developer"

#: landing/templates/landing/who_we_are.html:51
msgid "Desarrollador Back-end y lo que surja."
msgstr "Backend developer"

#: landing/templates/landing/who_we_are.html:59
msgid ""
"Interesado en encontrar ecoalternativas y asesorar sobre monedas sociales"
msgstr "Interested in ecoalternatives and advice on social currency "

#: landing/templates/landing/who_we_are.html:67
msgid "Community Manager"
msgstr "Community Manager"

#: landing/templates/landing/who_we_are.html:74
msgid "Marketing Mentor"
msgstr "Marketing Mentor"

#: landing/templates/landing/who_we_are.html:75
msgid "Barcelona"
msgstr "Barcelona"

#: landing/templates/landing/who_we_are.html:81
msgid ""
"Desarrollador full stack. Interesado en el software libre, Linux y nuevas "
"tecnologías"
msgstr ""
"Full stack developer. Interested in open software, Linux and new "
"technologies"

#: landing/templates/landing/who_we_are.html:90
msgid ""
"Front-end. Especializado en web performance, responsive web design y "
"software libre"
msgstr ""
"Front-end. Web performance oriented, responsive web design and open software"

#: landing/templates/landing/who_we_are.html:97
#: landing/templates/landing/who_we_are.html:125
msgid "Traducciones"
msgstr "Translations"

#: landing/templates/landing/who_we_are.html:102
msgid ""
"Translator & Fullstack Developer interested in Open Source projects and new "
"opportunities to learn"
msgstr "Translator and Full Stack developer"

#: landing/templates/landing/who_we_are.html:103
#: landing/templates/landing/who_we_are.html:117
msgid "Madrid"
msgstr ""

#: landing/templates/landing/who_we_are.html:109
msgid ""
"Economía Social y Solidaria, Economía Colaborativa y Economía Feminista"
msgstr ""

#: landing/templates/landing/who_we_are.html:110
msgid "Argentina - Madrid"
msgstr ""

#: landing/templates/landing/who_we_are.html:116
msgid "Product Owner"
msgstr ""

#: landing/templates/landing/who_we_are.html:133
msgid "Conoce donde nos reunimos"
msgstr "Where we meet"

#: landing/templates/landing/who_we_are.html:135
msgid ""
"El equipo de Semillas está deslocalizado, sin embargo gran parte de nosotros"
" estamos en Madrid. En este "
msgstr ""
"The Semillas team is remote, however, many of us live in Madrid. In this"

#: landing/templates/landing/who_we_are.html:135
msgid "puedes encontrar un video de nuestros primeros pasos como semilleros! "
msgstr "you can find a video of our first steps. "

#: landing/templates/landing/who_we_are.html:136
msgid "Si estáis interesados en contactarnos podéis escribirnos a "
msgstr "If you are interested in contacting, you can write us to"

#: landing/templates/landing/who_we_are.html:140
msgid ""
"\n"
"       Pensamos que otra forma de economía es posible. Pensamos que toda persona\n"
"       puede ofrecer algo y que eso debería ser suficiente para cubrir los mínimos de una vida digna.\n"
"       Diseñamos esta plataforma para ofrecer un medio, un soporte para un fin.\n"
"       Somos un grupo de hacktivistas, no hacemos esto por dinero.\n"
"       Buscamos todo tipo de ayuda. Ahora especialmente Android, iOS, diseño,\n"
"       frontend...\n"
"    "
msgstr ""
"\n"
"We think that another form of economy is possible. We think every person\n"
" has something to offer and that this fact should suffice in order to cover the bare minimum to make a decent livelihood.\n"
" We designed this platform to act as facilitators to make this possible. \n"
"We are a group of hacktivists and we don't do this for money. \n"
"We welcome all kinds of help, and right now especially with Android, iOS, design, \n"
"front-end..."

#: semillas_backend/templates/account/account_inactive.html:5
#: semillas_backend/templates/account/account_inactive.html:8
msgid "Account Inactive"
msgstr "Account Inactive "

#: semillas_backend/templates/account/account_inactive.html:10
msgid "This account is inactive."
msgstr "This account is inactive."

#: semillas_backend/templates/account/email.html:6
msgid "Account"
msgstr "Account"

#: semillas_backend/templates/account/email.html:9
msgid "E-mail Addresses"
msgstr "E-mail Addresses"

#: semillas_backend/templates/account/email.html:11
msgid "The following e-mail addresses are associated with your account:"
msgstr "The following e-mail addresses are associated with your account:"

#: semillas_backend/templates/account/email.html:25
msgid "Verified"
msgstr "Verified"

#: semillas_backend/templates/account/email.html:27
msgid "Unverified"
msgstr "Unverified"

#: semillas_backend/templates/account/email.html:29
msgid "Primary"
msgstr "Primary"

#: semillas_backend/templates/account/email.html:35
msgid "Make Primary"
msgstr "Make Primary"

#: semillas_backend/templates/account/email.html:36
msgid "Re-send Verification"
msgstr "Re-send Verification"

#: semillas_backend/templates/account/email.html:37
msgid "Remove"
msgstr "Remove"

#: semillas_backend/templates/account/email.html:44
msgid "Warning:"
msgstr "Warning:"

#: semillas_backend/templates/account/email.html:44
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, "
"etc."
msgstr ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, "
"etc."

#: semillas_backend/templates/account/email.html:49
msgid "Add E-mail Address"
msgstr "Add E-mail Address"

#: semillas_backend/templates/account/email.html:54
msgid "Add E-mail"
msgstr "Add E-mail"

#: semillas_backend/templates/account/email.html:63
msgid "Do you really want to remove the selected e-mail address?"
msgstr "Do you really want to remove the selected e-mail address?"

#: semillas_backend/templates/account/email_confirm.html:6
#: semillas_backend/templates/account/email_confirm.html:10
msgid "Confirm E-mail Address"
msgstr "Confirm E-mail Address"

#: semillas_backend/templates/account/email_confirm.html:16
#, python-format
msgid ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."
msgstr ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."

#: semillas_backend/templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "Confirm"

#: semillas_backend/templates/account/email_confirm.html:27
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please <a "
"href=\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr ""
"This e-mail confirmation link expired or is invalid. Please <a "
"href=\"%(email_url)s\">issue a new e-mail confirmation request</a>."

#: semillas_backend/templates/account/login.html:7
#: semillas_backend/templates/account/login.html:11
#: semillas_backend/templates/account/login.html:44
#: semillas_backend/templates/base.html:143
msgid "Sign In"
msgstr "Sign In"

#: semillas_backend/templates/account/login.html:16
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign up</a>\n"
"for a %(site_name)s account and sign in below:"

#: semillas_backend/templates/account/login.html:26
msgid "or"
msgstr "or"

#: semillas_backend/templates/account/login.html:33
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."

#: semillas_backend/templates/account/login.html:43
msgid "Forgot Password?"
msgstr "Forgot Password?"

#: semillas_backend/templates/account/logout.html:5
#: semillas_backend/templates/account/logout.html:8
#: semillas_backend/templates/account/logout.html:17
msgid "Sign Out"
msgstr "Sign Out"

#: semillas_backend/templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "Are you sure you want to sign out?"

#: semillas_backend/templates/account/password_change.html:6
#: semillas_backend/templates/account/password_change.html:9
#: semillas_backend/templates/account/password_change.html:14
#: semillas_backend/templates/account/password_reset_from_key.html:5
#: semillas_backend/templates/account/password_reset_from_key.html:8
#: semillas_backend/templates/account/password_reset_from_key_done.html:4
#: semillas_backend/templates/account/password_reset_from_key_done.html:7
msgid "Change Password"
msgstr "Change Password"

#: semillas_backend/templates/account/password_reset.html:7
#: semillas_backend/templates/account/password_reset.html:11
#: semillas_backend/templates/account/password_reset_done.html:6
#: semillas_backend/templates/account/password_reset_done.html:9
msgid "Password Reset"
msgstr "Password Reset"

#: semillas_backend/templates/account/password_reset.html:16
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you"
" an e-mail allowing you to reset it."
msgstr ""
"Forgotten your password? Enter your e-mail address below, and we'll send you"
" an e-mail allowing you to reset it."

#: semillas_backend/templates/account/password_reset.html:21
msgid "Reset My Password"
msgstr "Reset My Password"

#: semillas_backend/templates/account/password_reset.html:24
msgid "Please contact us if you have any trouble resetting your password."
msgstr "Please contact us if you have any trouble resetting your password."

#: semillas_backend/templates/account/password_reset_done.html:15
msgid ""
"We have sent you an e-mail. Please contact us if you do not receive it "
"within a few minutes."
msgstr ""
"We have sent you an e-mail. Please contact us if you do not receive it "
"within a few minutes."

#: semillas_backend/templates/account/password_reset_from_key.html:8
msgid "Bad Token"
msgstr "Bad Token"

#: semillas_backend/templates/account/password_reset_from_key.html:12
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password "
"reset</a>."
msgstr ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password "
"reset</a>."

#: semillas_backend/templates/account/password_reset_from_key.html:18
msgid "change password"
msgstr "change password"

#: semillas_backend/templates/account/password_reset_from_key.html:21
#: semillas_backend/templates/account/password_reset_from_key_done.html:8
msgid "Your password is now changed."
msgstr "Your password is now changed."

#: semillas_backend/templates/account/password_set.html:6
#: semillas_backend/templates/account/password_set.html:9
#: semillas_backend/templates/account/password_set.html:14
msgid "Set Password"
msgstr "Set Password"

#: semillas_backend/templates/account/signup.html:6
msgid "Signup"
msgstr "Signup"

#: semillas_backend/templates/account/signup.html:9
#: semillas_backend/templates/account/signup.html:20
#: semillas_backend/templates/base.html:138
msgid "Sign Up"
msgstr "Sign Up"

#: semillas_backend/templates/account/signup.html:11
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."

#: semillas_backend/templates/account/signup.html:12
msgid ""
"We are not live yet, but we can keep you updated of our improvements and "
"notify you once we launch."
msgstr ""
"We are not live yet, but we can keep you updated on our progress and notify "
"you once we launch."

#: semillas_backend/templates/account/signup_closed.html:5
#: semillas_backend/templates/account/signup_closed.html:8
msgid "Sign Up Closed"
msgstr "Sign Up Closed"

#: semillas_backend/templates/account/signup_closed.html:10
msgid ""
"Thanks a lot for joining. We will keep you posted. Keep reading about us"
msgstr ""
"Thanks a lot for joining. We will keep you posted. Keep reading about us"

#: semillas_backend/templates/account/verification_sent.html:5
#: semillas_backend/templates/account/verification_sent.html:8
#: semillas_backend/templates/account/verified_email_required.html:5
#: semillas_backend/templates/account/verified_email_required.html:8
msgid "Verify Your E-mail Address"
msgstr "Verify Your E-mail Address"

#: semillas_backend/templates/account/verification_sent.html:10
msgid ""
"We have sent an e-mail to you for verification. Follow the link provided to "
"finalize the signup process. Please contact us if you do not receive it "
"within a few minutes."
msgstr ""
"We have sent an e-mail to you for verification. Follow the link provided to "
"finalize the signup process. Please contact us if you do not receive it "
"within a few minutes."

#: semillas_backend/templates/account/verified_email_required.html:12
msgid ""
"This part of the site requires us to verify that\n"
"you are who you claim to be. For this purpose, we require that you\n"
"verify ownership of your e-mail address. "
msgstr ""
"This part of the site requires us to verify that\n"
"you are who you claim to be. For this purpose, we require that you\n"
"verify ownership of your e-mail address. "

#: semillas_backend/templates/account/verified_email_required.html:16
msgid ""
"We have sent an e-mail to you for\n"
"verification. Please click on the link inside this e-mail. Please\n"
"contact us if you do not receive it within a few minutes."
msgstr ""
"We have sent an e-mail to you for\n"
"verification. Please click on the link inside this e-mail. Please\n"
"contact us if you do not receive it within a few minutes."

#: semillas_backend/templates/account/verified_email_required.html:20
#, python-format
msgid ""
"<strong>Note:</strong> you can still <a href=\"%(email_url)s\">change your "
"e-mail address</a>."
msgstr ""
"<strong>Note:</strong> you can still <a href=\"%(email_url)s\">change your "
"e-mail address</a>."

#: semillas_backend/templates/base.html:109
#: semillas_backend/templates/base.html:110
msgid "Por qué usar Semillas"
msgstr "Why to use Semillas"

#: semillas_backend/templates/base.html:113
#: semillas_backend/templates/base.html:114
msgid "Quiénes Somos"
msgstr "The Team"

#: semillas_backend/templates/base.html:127
msgid "My Profile"
msgstr "My Profile"

#: semillas_backend/templates/base.html:132
msgid "Sign out"
msgstr "Sign out"

#: semillas_backend/templates/base.html:193
msgid "Contacta con nosotros"
msgstr "Contact"

#: semillas_backend/templates/base.html:198
msgid "Síguenos"
msgstr "Follow us"

#: semillas_backend/templates/base.html:214
msgid "Subir"
msgstr "Up"

#: semillas_backend/users/models.py:24
msgid "Name of User"
msgstr "Name of User"

#: semillas_backend/users/validators.py:10
#, python-format
msgid "%(value)s Does not start with @"
msgstr "%(value)s debe empezar con '@'"

#: semillas_backend/users/validators.py:20
msgid "Multiple lines in the Faircoin address"
msgstr ""

#: semillas_backend/users/validators.py:25
msgid "Spaces in the Faircoin address"
msgstr ""

#: semillas_backend/users/validators.py:30
msgid "Faircoin format not correct"
msgstr ""

#: wallet/models.py:91
msgid "You just received a transaction"
msgstr ""

#: wallet/models.py:92
#, python-brace-format
msgid "You just received {amount} {currency} from {sender}"
msgstr ""

#: wallet/models.py:104
msgid "You just send a transaction"
msgstr ""

#: wallet/models.py:105
#, python-brace-format
msgid "You just sent {amount} {currency} to {recipient}"
msgstr ""

#: wallet/views.py:48
msgid "Value can't be empty"
msgstr "La cantidad no puede estar vacía"

#: wallet/views.py:55
msgid "Transaction created correctly!"
msgstr ""

#: wallet/views.py:56
msgid "The transaction was not created correctly!"
msgstr ""

#: wallet/views.py:58
msgid "Not enough balance to make that transaction"
msgstr ""

#: wallet/views.py:59
msgid "Source and destination wallets can not be the same!"
msgstr ""
